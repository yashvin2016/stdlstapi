﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentDetailAPI.DataBase
{
    public class StudentDataRepo
    {
        public static List<StudentDataRepo> stdListData = new List<StudentDataRepo>();
        public string Name { get; set; }
        public string Id { get; set; }

       

        public List<StudentDataRepo> GetStudentList()
        {
            stdListData.Add(new StudentDataRepo { Name = "Yashvin", Id = "1" });
            stdListData.Add(new StudentDataRepo { Name = "Sumo" , Id = "2"});
            stdListData.Add(new StudentDataRepo { Name = "BMW", Id = "3" });
            stdListData.Add(new StudentDataRepo { Name = "Husky", Id = "4" });

            return stdListData;
        }
    }
}