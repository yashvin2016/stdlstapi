﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using StudentDetailAPI.BusinessLayer;
using StudentDetailAPI.MapperClass;

namespace StudentDetailAPI.Controllers
{
    public class StdDetailController : ApiController
    {
        // GET api/stddetail
        public IEnumerable<DataMapper> Get()
        {
            StudentDetailRecord StdRecord = new StudentDetailRecord();
            List<DataMapper> ListOfStd =  StdRecord.GetAllStudent();
            return  ListOfStd ;
                   
        }

        // GET api/stddetail/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/stddetail
        [HttpPost]
        public void Post(DataMapper dataObj)
        {
            StudentDetailRecord StdRecord = new StudentDetailRecord();
            StdRecord.AddStudents(dataObj);
        }

        // PUT api/stddetail/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/stddetail/5
        public void Delete(int id)
        {
        }
    }
}
