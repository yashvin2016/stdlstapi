﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StudentDetailAPI.Interface;
using StudentDetailAPI.DataBase;
using StudentDetailAPI.MapperClass;

namespace StudentDetailAPI.BusinessLayer
{
    public class StudentDetailRecord : IStudentRecord
    {
        public string Name { get; set; }
        public string Id { get; set; }

        public List<DataMapper> GetAllStudent()
        {
            StudentDataRepo StdRepo = new StudentDataRepo();
 
            List<StudentDataRepo> StdList = StdRepo.GetStudentList();
            List<DataMapper> stdList = new List<DataMapper>();

            for(int i =0 ; i < StdList.Count ; i ++)
            {
                stdList.Add(new DataMapper{FName = StdList[i].Name, MId = StdList[i].Id});
            }
            return stdList;
        }

        public void AddStudents(DataMapper Std)
        {

            StudentDataRepo.stdListData.Add(new StudentDataRepo { Name = Std.FName, Id = Std.MId });
        }
    }
}