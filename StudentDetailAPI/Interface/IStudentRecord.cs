﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentDetailAPI.MapperClass;
namespace StudentDetailAPI.Interface
{
    interface IStudentRecord
    {
        List<DataMapper> GetAllStudent();
        void AddStudents(DataMapper Std);
    }
}
